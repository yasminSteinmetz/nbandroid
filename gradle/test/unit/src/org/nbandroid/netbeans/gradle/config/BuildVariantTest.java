package org.nbandroid.netbeans.gradle.config;

import com.android.builder.model.AndroidProject;
import com.android.builder.model.Variant;
import com.google.common.collect.Lists;
import java.util.List;
import org.junit.Test;

import static org.junit.Assert.*;
import org.junit.Before;
import static org.mockito.Mockito.*;
import org.netbeans.spi.project.AuxiliaryProperties;

public class BuildVariantTest {
  
  private AuxiliaryProperties auxProps;
  
  @Before
  public void before() {
    auxProps = mock(AuxiliaryProperties.class);
  }
  
  @Test
  public void testNoProject() {
    BuildVariant bv = new BuildVariant(auxProps);
    assertNull(bv.getVariantName());
    assertNull(bv.getCurrentVariant());
    assertNull(bv.getCurrentBuildTypeContainer());
  }

  private static Variant createVariant(String name) {
    Variant variant = mock(Variant.class);
    when(variant.getName()).thenReturn(name);
    return variant;
  }
  
  @Test
  public void defaultConfig() {
    AndroidProject aPrj = mock(AndroidProject.class);
    Variant dbgVariant = createVariant("debug");
    Variant relVariant = createVariant("release");
    List<Variant> variants = Lists.newArrayList(dbgVariant, relVariant);
    when(aPrj.getVariants()).thenReturn(variants);
    
    BuildVariant bv = new BuildVariant(auxProps);
    bv.setAndroidProject(aPrj);
    assertEquals("debug", bv.getVariantName());
    assertEquals(dbgVariant, bv.getCurrentVariant());
  }
  
  @Test
  public void flavors() {
    AndroidProject aPrj = mock(AndroidProject.class);
    Variant dbg1Variant = createVariant("f1Debug");
    Variant dbg2Variant = createVariant("f2Debug");
    List<Variant> variants = Lists.newArrayList(
        dbg1Variant, createVariant("f1Release"), dbg2Variant, createVariant("f2Release"));
    when(aPrj.getVariants()).thenReturn(variants);
    
    BuildVariant bv = new BuildVariant(auxProps);
    bv.setAndroidProject(aPrj);
    assertEquals("f1Debug", bv.getVariantName());
    assertEquals(dbg1Variant, bv.getCurrentVariant());
    
    bv.setVariantName("not-existing-variant");
    assertEquals("f1Debug", bv.getVariantName());
    assertEquals(dbg1Variant, bv.getCurrentVariant());
    
    bv.setVariantName("f2Debug");
    assertEquals("f2Debug", bv.getVariantName());
    assertEquals(dbg2Variant, bv.getCurrentVariant());
  }
}