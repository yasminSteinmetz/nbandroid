package org.nbandroid.netbeans.gradle;

import com.google.common.collect.Iterables;
import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.project.Project;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.project.spi.DalvikPlatformResolver;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

import org.netbeans.api.java.project.JavaProjectConstants;
import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.api.project.Sources;
import org.netbeans.gradle.project.api.task.BuiltInGradleCommandQuery;
import org.netbeans.junit.MockServices;
import org.netbeans.modules.android.project.api.AndroidConstants;
import org.netbeans.modules.android.project.api.AndroidManifestSource;
import org.netbeans.modules.android.project.api.AndroidProjects;
import org.netbeans.spi.java.queries.SourceForBinaryQueryImplementation;

import static org.nbandroid.netbeans.gradle.GradleProjectFixture.*;

public class DependenciesGradleProjectTest {
  private static final Logger LOG = Logger.getLogger(DependenciesGradleProjectTest.class.getName());

  private static PluginsFixture pluginsFix;
  private static GradleProjectFixture prjFix;
  private static Project lib;
  private static FileObject foProjectSrc;

  @BeforeClass
  public static void setUpClass() throws Exception {
    MockServices.setServices();
    pluginsFix = new PluginsFixture().setupSDK().setupGradle();

    prjFix = new GradleProjectFixture.Builder()
        .usePrefix(GradleTests.TEST_ARCHIVE_DIR)
        .useName("dependencies")
        .useIs(GradleTests.testArchiveStream())
        .create();
    lib = prjFix.loadProject("jarProject");
    
    foProjectSrc = prjDir(prjFix).getFileObject("src/main/java");
  }

  @AfterClass
  public static void clear() {
    prjFix.tearDown();
  }

  @Test
  public void isAndroidProject() throws Exception {
    assertTrue(AndroidProjects.isAndroidProject(prjFix.prj));
    assertFalse(AndroidProjects.isAndroidProject(lib));
  }
  
  @Test
  public void basic() throws Exception {
    // get the classpath
    verifyClasspath(prjFix, foProjectSrc, ClassPath.SOURCE, 
        "dependencies/src/main/java" 
        // "tictactoe/app/build/source/r/debug" - will be added after build
        );
    // TODO should jarProject.jar be on classpath?
    // verifyClasspath(prjFix, foProjectSrc, ClassPath.COMPILE, 
        // "dependencies/libs/jarProject.jar"
    //    );
    verifyClasspath(prjFix, foProjectSrc, ClassPath.BOOT, "android.jar");
    
    DalvikPlatformResolver platformProvider = prjLookup(prjFix).lookup(DalvikPlatformResolver.class);
    assertNotNull(platformProvider);
    DalvikPlatform platform = platformProvider.findDalvikPlatform();
    assertNotNull(platform);
  }
  
  // TODO
  // @Test
  public void sourceForAndroidLibraryDependency() {
    URL libRoot = FileUtil.urlForArchiveOrDir(new File(prjFix.prjDir, "libs/jarProject.jar"));
    SourceForBinaryQueryImplementation sfb = prjLookup(prjFix).lookup(SourceForBinaryQueryImplementation.class);
    SourceForBinaryQuery.Result libSources = sfb.findSourceRoots(libRoot);
    assertNotNull(libSources);
    // check there is main source root from library
    assertTrue(Arrays.toString(libSources.getRoots()),
        Arrays.asList(libSources.getRoots()).contains(lib.getProjectDirectory().getFileObject("src/main/java")));
  }
  
  @Test
  public void sources() throws Exception {
    Sources sources = ProjectUtils.getSources(prjFix.prj);
    assertNotNull(sources);

    final FileObject foSrc = prjDir(prjFix).getFileObject("src/main/java/com/android/tests/dependencies/MainActivity.java");
    SourceGroup[] sourceGroups = sources.getSourceGroups(JavaProjectConstants.SOURCES_TYPE_JAVA);
    assertTrue("java source in " + sources,
        Iterables.any(
            Arrays.asList(sourceGroups), 
            sourceGroupContainsFile(foSrc)));
    final FileObject foRes = prjDir(prjFix).getFileObject("src/main/res/layout/main.xml");
    SourceGroup[] sourceGroups2 = sources.getSourceGroups(AndroidConstants.SOURCES_TYPE_ANDROID_RES);
    assertTrue("app resource in " + sources,
        Iterables.any(
            Arrays.asList(sourceGroups2), 
            sourceGroupContainsFile(foRes)));
  }
  
  @Test
  public void manifest() throws Exception {
    AndroidManifestSource ams = prjLookup(prjFix).lookup(AndroidManifestSource.class);
    assertNotNull(ams);

    FileObject foSrc = prjDir(prjFix).getFileObject("src/main/" + AndroidConstants.ANDROID_MANIFEST_XML);
    assertEquals(foSrc, ams.get());
  }
  
  @Test
  public void commands() throws Exception {
    BuiltInGradleCommandQuery commands = prjLookup(prjFix).lookup(BuiltInGradleCommandQuery.class);
    assertNotNull(commands);
  }
}