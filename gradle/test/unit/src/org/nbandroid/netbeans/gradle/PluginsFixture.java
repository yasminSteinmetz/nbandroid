package org.nbandroid.netbeans.gradle;

import java.io.File;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.gradle.project.properties.GradleLocationDef;
import org.netbeans.gradle.project.properties.global.GlobalGradleSettings;
import org.netbeans.gradle.project.properties.GradleLocationDirectory;
import org.netbeans.modules.android.core.sdk.DalvikPlatformManager;

/**
 *
 * @author radim
 */
public class PluginsFixture {
  private static final String SDK_DIR = System.getProperty("test.all.android.sdks.home");
  private static final String GRADLE_DIR = System.getProperty("test.all.gradle.home");

  private DalvikPlatformManager platformManager;

  public PluginsFixture setupGradle() {
    GlobalGradleSettings.getDefault().gradleLocation().setValue(new GradleLocationDef(new GradleLocationDirectory(new File(GRADLE_DIR)), false));
    GlobalGradleSettings.getDefault().gradleJdk().setValue(JavaPlatform.getDefault());
    return this;
  }
  
  public PluginsFixture setupSDK() {
    platformManager = DalvikPlatformManager.getDefault();
    platformManager.setSdkLocation(SDK_DIR);
    return this;
  }

  public DalvikPlatformManager getPlatformManager() {
    return platformManager;
  }
}
