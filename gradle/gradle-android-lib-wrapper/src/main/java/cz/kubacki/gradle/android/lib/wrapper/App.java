package cz.kubacki.gradle.android.lib.wrapper;

import com.android.builder.model.AndroidArtifact;
import com.android.builder.model.AndroidProject;
import com.android.builder.model.AndroidLibrary;
import com.android.builder.model.BuildTypeContainer;
import com.android.builder.model.JavaArtifact;
import com.android.builder.model.JavaLibrary;
import com.android.builder.model.Variant;
import java.io.File;
import org.gradle.tooling.GradleConnector;
import org.gradle.tooling.ProjectConnection;
import org.gradle.tooling.model.GradleProject;
import org.gradle.tooling.model.idea.IdeaContentRoot;
import org.gradle.tooling.model.idea.IdeaDependency;
import org.gradle.tooling.model.idea.IdeaModule;
import org.gradle.tooling.model.idea.IdeaProject;
import org.gradle.tooling.model.idea.IdeaSourceDirectory;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) {
        GradleConnector connector = GradleConnector.newConnector();
        connector.useGradleVersion("1.12");
        for (String path : args) {
          scanProject(path, connector);
        }
    }
    
    public static void scanProject(String path, GradleConnector connector) {

        // File projectDir = new File("/home/radim/Projects/tictactoe/app");
        // File projectDir = new File("/home/radim/NetBeansProjects/gradle-samples-0.6.1/basic");
        // File projectDir = new File("/home/radim/src/github.com/RobolectricSample");
        connector.forProjectDirectory(new File(path));

        ProjectConnection connection = connector.connect();
        GradleProject gPrj = connection.getModel(GradleProject.class);
        System.out.println("Gradle project: " + gPrj + ", " + gPrj.getClass().getName());
        // Load the custom model for the project
        AndroidProject model = connection.getModel(AndroidProject.class);
        // dumpVariant("<current>", model.getDefaultConfig().getTestSourceProvider());
        System.err.println("model " + model);
        for (Variant variant : model.getVariants()) {
          dumpVariant(variant);
        }
        for (BuildTypeContainer buildType : model.getBuildTypes()) {
          System.err.println(" build type container " + buildType.getBuildType().getName());
          for (File f : buildType.getSourceProvider().getJavaDirectories()) {
              System.err.println("  java dir " + f);
          }
        }
        IdeaProject prj = connection.getModel(IdeaProject.class);
        System.err.println("model " + prj);
        for (IdeaModule m : prj.getModules().getAll()) {
            System.err.println(" module " + m);
            for (IdeaDependency d : m.getDependencies().getAll()) {
                System.err.println("  dep " + d);
            }
        }
        
        scanIdea(connection);
    }
    public static void dumpVariant(Variant variant) {
      System.err.println(" variant " + variant.getDisplayName() + " (" + variant.getName() + ")");
      for (AndroidLibrary l : variant.getMainArtifact().getDependencies().getLibraries()) {
        System.err.println("  lib " + l);
      }
      for (AndroidArtifact aa : variant.getExtraAndroidArtifacts()) {
        System.err.println("  extra android artifact " + aa.getName());
        for (AndroidLibrary l : aa.getDependencies().getLibraries()) {
          System.err.println("   test lib " + l);
        }
        for (String s : aa.getDependencies().getProjects()) {
          System.err.println("   test project dependency " + s);
        }
        for (JavaLibrary javaLib : aa.getDependencies().getJavaLibraries()) {
          System.err.println("   test JAR dependency " + javaLib.getJarFile().getAbsolutePath());
        }
      }
      for (JavaArtifact aa : variant.getExtraJavaArtifacts()) {
        System.err.println("  extra Java artifact " + aa.getName());
        if (aa.getDependencies() != null) {
          for (AndroidLibrary l : aa.getDependencies().getLibraries()) {
            System.err.println("   test lib " + l);
          }
          for (String s : aa.getDependencies().getProjects()) {
            System.err.println("   test project dependency " + s);
          }
          for (JavaLibrary javaLib : aa.getDependencies().getJavaLibraries()) {
            System.err.println("   test JAR dependency " + javaLib.getJarFile().getAbsolutePath());
          }
        } else {
          System.err.println("   extra Java artifact deps NULL!!");
        }
      }
    }

  private static void scanIdea(ProjectConnection connection) {
      IdeaProject model = connection.getModel(IdeaProject.class);
      System.out.println("IDEA model: " + model);
      for (IdeaModule o : model.getModules()) {
        System.out.println("module: " + o);
        for (IdeaContentRoot root : o.getContentRoots()) {
          System.out.println("content root: " + root.getRootDirectory());
          for (IdeaSourceDirectory src : root.getSourceDirectories()) {
            System.out.println(" src dir " + src.getDirectory().getPath());
          }
          for (IdeaSourceDirectory src : root.getTestDirectories()) {
            System.out.println(" test dir " + src.getDirectory().getPath());
          }
        }
      }
  }
}
