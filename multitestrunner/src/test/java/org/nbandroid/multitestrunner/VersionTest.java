package org.nbandroid.multitestrunner;

import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

@RunWith(MultiVersionSpecRunner.class)
@TargetVersions({"1", "2"})
public class VersionTest {
  private static final ThreadLocal<String> VERSION = new ThreadLocal<String>();

  public static void setVersion(String version) {
    VERSION.set(version);
  }

  @Test
  public void version() {
    String v = VERSION.get();
    assertTrue(v.equals("1") || v.equals("2"));
  }
}
