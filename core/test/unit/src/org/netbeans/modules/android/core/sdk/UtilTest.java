/*
 *  Copyright 2009 radim.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */

package org.netbeans.modules.android.core.sdk;

import java.io.IOException;
import org.junit.Test;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import static org.junit.Assert.*;
import org.nbandroid.netbeans.test.AbstractAndroidPlatformTest;
import org.nbandroid.netbeans.test.PlatformVersions;

@PlatformVersions
public class UtilTest extends AbstractAndroidPlatformTest {

  @Test
  public void findToolIn() throws IOException {
    FileObject sdkDir = Utils.createSdk();
    for (Tool t : Util.sdkTools()) {
      FileObject toolFO = Util.findTool(
          t.getSystemName(), sdkDir.getFileObject(getPlatform().platformDir));
      assertNotNull(t.getSystemName() + " found in " + getPlatform(), toolFO);
      assertTrue(t.getSystemName() + " found in " + getPlatform(),
          FileUtil.isParentOf(sdkDir, toolFO));
    }
  }

  @Test
  public void findResourceIn() throws IOException {
    FileObject sdkDir = Utils.createSdk();
    for (Tool t : Util.sdkResources()) {

      FileObject toolFO = Util.findTool(
          t.getSystemName(), sdkDir.getFileObject(getPlatform().platformDir));
      assertNotNull(t.getSystemName() + " found in " + getPlatform(), toolFO);
      assertTrue(t.getSystemName() + " found in " + getPlatform(),
          FileUtil.isParentOf(sdkDir, toolFO));
    }
  }
}