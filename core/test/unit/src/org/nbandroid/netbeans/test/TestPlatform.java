/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.nbandroid.netbeans.test;

// XXX this is repeating something from SDK

public enum TestPlatform {
  ANDROID_5_0(21, "android-21", "platforms/android-21"),
  ANDROID_WITH_GOOGLE_API_5_0W(21, "Google Inc.:Google APIs:21", "add-ons/addon-google_apis-google-21", "platforms/android-21"),
  ANDROID_4_4W(20, "android-20", "platforms/android-20"),
  // TODO(radim): add Glass developent kit platform and test for gdk.jar
//  ANDROID_WITH_GOOGLE_API_4_4W(20, "Google Inc.:Glass Development Kit Preview:19"/*"Google Inc.:Google APIs:20"*/,
//      "add-ons/addon-google_apis-google-20", "platforms/android-20"),
  ANDROID_4_4_2(19, "android-19", "platforms/android-19"),
  ANDROID_WITH_GOOGLE_API_4_4_2(19, "Google Inc.:Google APIs:19", "add-ons/addon-google_apis-google-19", "platforms/android-19"),
  ANDROID_4_3(18, "android-18", "platforms/android-18"), 
  ANDROID_WITH_GOOGLE_API_4_3(18, "Google Inc.:Google APIs:18", "add-ons/addon-google_apis-google-18", "platforms/android-18"), 
  ANDROID_4_2(17, "android-17", "platforms/android-17"), 
  ANDROID_WITH_GOOGLE_API_4_2(17, "Google Inc.:Google APIs:17", "add-ons/addon-google_apis-google-17", "platforms/android-17"), 
  ANDROID_4_1(16, "android-16", "platforms/android-16"), 
  ANDROID_WITH_GOOGLE_API_4_1(16, "Google Inc.:Google APIs:16", "add-ons/addon-google_apis-google-16", "platforms/android-16"), 
  ANDROID_4_0_3(15, "android-15", "platforms/android-15"), 
  ANDROID_WITH_GOOGLE_API_4_0_3(15, "Google Inc.:Google APIs:15", "add-ons/addon-google_apis-google-15", "platforms/android-15"), 
  ANDROID_2_3_3(10, "android-10", "platforms/android-10"), 
  ANDROID_WITH_GOOGLE_API_2_3_3(10, "Google Inc.:Google APIs:10", "add-ons/addon-google_apis-google-10", "platforms/android-10"), 
  ANDROID_2_2(8, "android-8", "platforms/android-8"), 
  ANDROID_WITH_GOOGLE_API_2_2(8, "Google Inc.:Google APIs:8", "add-ons/addon-google_apis-google-8", "platforms/android-8");
  
  public final int apiLevel;
  public final String target;
  public final String installDir;
  public final String platformDir;

  private TestPlatform(int apiLevel, String target, String installDir) {
    this(apiLevel, target, installDir, installDir);
  }

  private TestPlatform(int apiLevel, String target, String installDir, String platformDir) {
    this.apiLevel = apiLevel;
    this.target = target;
    this.installDir = installDir;
    this.platformDir = platformDir;
  }
  
  public boolean isPurePlatform() {
    return installDir.equals(platformDir);
  }

  @Override
  public String toString() {
    return "TestPlatform{" + "target=" + target + ", installDir=" + installDir + ", platformDir=" + platformDir + '}';
  }
  
}
