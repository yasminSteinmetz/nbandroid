/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.nbandroid.netbeans.test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.nbandroid.multitestrunner.AbstractMultiTestRunner;

/**
 *
 * @author radim
 */
public class AndroidPlatformTestSuiteRunner extends AbstractMultiTestRunner {

  public AndroidPlatformTestSuiteRunner(Class<?> target) {
    super(target);
  }

  @Override
  protected void createExecutions() {
    PlatformVersions versions = target.getAnnotation(PlatformVersions.class);
    if (Modifier.isAbstract(target.getModifiers())) {
      return;
    }
    if (versions != null) {
      for (TestPlatform platform : TestPlatform.values()) {
        if (matchesType(versions.type(), platform)
            && matchesLevel(versions.minimumVersion(), versions.maximumVersion(), platform)) {
          add(new PlatformExecution(platform));
        }
      }
    } else {
      throw new RuntimeException("Target class " + target.getName() + " is not annotated with @PlatformExecution.");
    }
  }
  
  private static boolean matchesType(PlatformVersions.PlatformType type, TestPlatform platform) {
    if (type == null) {
      return true;
    }
    switch (type) {
      case ALL:
        return true;
      case PURE:
        return platform.isPurePlatform();
      case EXTENSION:
        return !platform.isPurePlatform();
      default:
        throw new RuntimeException("Unknown platform type annotation " + type);
    }
  }
    
  private static boolean matchesLevel(int minLevel, int maxLevel, TestPlatform platform) {
    return minLevel <= platform.apiLevel && platform.apiLevel <= maxLevel;
  }
    
  private static class PlatformExecution extends AbstractMultiTestRunner.Execution {

    private final TestPlatform platform;

    PlatformExecution(TestPlatform platform) {
      this.platform = platform;
    }

    @Override
    protected String getDisplayName() {
      return platform.target;
    }

    @Override
    protected boolean isTestEnabled(TestDetails testDetails) {
      PlatformVersions versions = testDetails.getAnnotation(PlatformVersions.class);
      if (versions == null) {
        return true;
      }
      return matchesType(versions.type(), platform)
          && matchesLevel(versions.minimumVersion(), versions.maximumVersion(), platform);
    }

    @Override
    protected void before() {
      try {
        Method method = target.getMethod("setPlatform", TestPlatform.class);
        method.invoke(null, platform);
      } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
        throw new RuntimeException("Cannot set platform to target class " + target.getName() + ".", ex);
      }
    }
  }
}
