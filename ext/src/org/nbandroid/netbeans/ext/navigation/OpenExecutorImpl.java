package org.nbandroid.netbeans.ext.navigation;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import javax.swing.text.StyledDocument;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.LineCookie;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.text.Line;
import org.openide.text.NbDocument;
import org.openide.util.NbBundle;
import org.openide.util.UserQuestionException;

class OpenExecutorImpl implements UiUtilsCallerImpl.OpenExecutor {
  private static final Logger LOG = Logger.getLogger(OpenExecutorImpl.class.getName());
  
  @Override
  public boolean doOpen(FileObject fo) {
    try {
      DataObject od = DataObject.find(fo);
      EditorCookie ec = od.getLookup().lookup(EditorCookie.class);
      LineCookie lc = od.getLookup().lookup(LineCookie.class);

      /* if (ec != null && lc != null && offset != -1) {
        StyledDocument doc;
        try {
          doc = ec.openDocument();
        } catch (UserQuestionException uqe) {
          final Object value = DialogDisplayer.getDefault().notify(
              new NotifyDescriptor.Confirmation(uqe.getLocalizedMessage(),
              NbBundle.getMessage(UiUtilsCallerImpl.class, "TXT_Question"),
              NotifyDescriptor.YES_NO_OPTION));
          if (value != NotifyDescriptor.YES_OPTION) {
            return false;
          }
          uqe.confirmed();
          doc = ec.openDocument();
        }
        if (doc != null) {
          int line = NbDocument.findLineNumber(doc, offset);
          int lineOffset = NbDocument.findLineOffset(doc, line);
          int column = offset - lineOffset;

          if (line != -1) {
            Line l = lc.getLineSet().getCurrent(line);

            if (l != null) {
              doShow(l, column);
              return true;
            }
          }
        }
      }*/

      OpenCookie oc = od.getLookup().lookup(OpenCookie.class);

      if (oc != null) {
        doOpen(oc);
        return true;
      }
    } catch (IOException e) {
      if (LOG.isLoggable(Level.INFO)) {
        LOG.log(Level.INFO, e.getMessage(), e);
      }
    }

    return false;
  }
  @Override
  public boolean doOpenLine(FileObject fo, int line) {
    try {
      DataObject od = DataObject.find(fo);
      EditorCookie ec = od.getLookup().lookup(EditorCookie.class);
      LineCookie lc = od.getLookup().lookup(LineCookie.class);

      if (ec != null && lc != null && line != -1) {
        StyledDocument doc;
        try {
          doc = ec.openDocument();
        } catch (UserQuestionException uqe) {
          final Object value = DialogDisplayer.getDefault().notify(
              new NotifyDescriptor.Confirmation(uqe.getLocalizedMessage(),
              NbBundle.getMessage(UiUtilsCallerImpl.class, "TXT_Question"),
              NotifyDescriptor.YES_NO_OPTION));
          if (value != NotifyDescriptor.YES_OPTION) {
            return false;
          }
          uqe.confirmed();
          doc = ec.openDocument();
        }
        if (doc != null) {
          int column = 0;
          if (line != -1) {
            Line l = lc.getLineSet().getCurrent(line);

            if (l != null) {
              doShow(l, column);
              return true;
            }
          }
        }
      }

      OpenCookie oc = od.getLookup().lookup(OpenCookie.class);

      if (oc != null) {
        doOpen(oc);
        return true;
      }
    } catch (IOException e) {
      if (LOG.isLoggable(Level.INFO)) {
        LOG.log(Level.INFO, e.getMessage(), e);
      }
    }

    return false;
  }

  private static void doShow(final Line l, final int column) {
    if (SwingUtilities.isEventDispatchThread()) {
      l.show(Line.ShowOpenType.OPEN, Line.ShowVisibilityType.FOCUS, column);
    } else {
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          l.show(Line.ShowOpenType.OPEN, Line.ShowVisibilityType.FOCUS, column);
        }
      });
    }
  }

  private static void doOpen(final OpenCookie oc) {
    if (SwingUtilities.isEventDispatchThread()) {
      oc.open();
    } else {
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          oc.open();
        }
      });
    }
  }
}
