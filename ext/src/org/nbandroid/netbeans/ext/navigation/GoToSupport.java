package org.nbandroid.netbeans.ext.navigation;

import com.google.common.collect.Sets;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.Scope;
import com.sun.source.tree.Tree;
import com.sun.source.tree.Tree.Kind;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.AbstractElementVisitor6;
import javax.lang.model.util.ElementFilter;
import javax.swing.text.Document;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.JavaSource.Phase;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.modules.android.project.api.ResourceRef;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser.Result;
import org.openide.filesystems.FileObject;
import org.openide.util.NbBundle;

/**
 *
 * @author Jan Lahoda
 */
public class GoToSupport {

  private final UiUtilsCaller callback;

  GoToSupport(UiUtilsCaller callback) {
    this.callback = callback;
  }
  
    
    public static String getGoToElementTooltip(final Document doc, final int offset) {
      try {
        final FileObject fo = EditorUtilities.getFileObject(doc);

        if (fo == null) {
          return null;
        }

        final String[] result = new String[1];

        ParserManager.parse(Collections.singleton(Source.create(doc)), new UserTask() {
          @Override
          public void run(ResultIterator resultIterator) throws Exception {
            Result res = resultIterator.getParserResult(offset);
            CompilationController controller = res != null ? CompilationController.get(res) : null;
            if (controller == null || controller.toPhase(Phase.RESOLVED).compareTo(Phase.RESOLVED) < 0) {
              return;
            }

            Context resolved = resolveContext(controller, doc, offset);

            if (resolved != null) {
              result[0] = computeTooltip(controller, resolved);
            }
          }
        });

        return result[0];
      } catch (ParseException ex) {
        throw new IllegalStateException(ex);
      }
    }
    
  private static boolean isError(Element el) {
    return el == null || el.asType() == null || el.asType().getKind() == TypeKind.ERROR;
  }
    
  private void performGoTo(final Document doc, final int offset) {
    final AtomicBoolean cancel = new AtomicBoolean();
    ProgressUtils.runOffEventDispatchThread(new Runnable() {
      @Override
      public void run() {
        performGoToImpl(doc, offset, cancel);
      }
    }, NbBundle.getMessage(GoToSupport.class, "LBL_GoToResource"), cancel, false);
  }

  private void performGoToImpl(final Document doc, final int offset, final AtomicBoolean cancel) {
    try {
      final FileObject fo = EditorUtilities.getFileObject(doc);

      if (fo == null) {
        return;
      }

      final ElementHandle[] elementToOpen = new ElementHandle[1];
      final String[] displayNameForError = new String[1];
      final boolean[] tryToOpen = new boolean[1];

      ParserManager.parse(Collections.singleton(Source.create(doc)), new UserTask() {
        @Override
        public void run(ResultIterator resultIterator) throws Exception {
          Result res = resultIterator.getParserResult(offset);
          if (cancel != null && cancel.get()) {
            return;
          }
          CompilationController controller = res != null ? CompilationController.get(res) : null;
          if (controller == null || controller.toPhase(Phase.RESOLVED).compareTo(Phase.RESOLVED) < 0) {
            return;
          }

          Context resolved = resolveContext(controller, doc, offset);

          if (resolved == null) {
            callback.beep();
            return;
          }

          elementToOpen[0] = ElementHandle.create(resolved.resolved);
          displayNameForError[0] = Utilities.getElementName(resolved.resolved, false).toString();
          tryToOpen[0] = true;
        }
      });

      if (tryToOpen[0]) {
        boolean openSucceeded = false;

        if (cancel.get()) {
          return;
        }

        if (elementToOpen[0] != null) {
          String[] jvmSignature = SourceUtils.getJVMSignature(elementToOpen[0]);
          if (jvmSignature.length == 3) {
            int idx = jvmSignature[0].indexOf(".R$");
            // TODO find if it is 'same package'
            if (idx > 0) {
              openSucceeded = callback.open(
                  fo,
                  new ResourceRef(
                      true, jvmSignature[0].substring(0, idx), 
                      jvmSignature[0].substring(idx + ".R$".length()), 
                      jvmSignature[1],
                      null));
            }
          }
        }
        if (!openSucceeded) {
          callback.warnCannotOpen(displayNameForError[0]);
        }
      }
    } catch (ParseException ex) {
      throw new IllegalStateException(ex);
    }
  }
    
  public void goTo(final Document doc, final int offset) {
    performGoTo(doc, offset);
  }
    
  public static Context resolveContext(CompilationInfo controller, Document doc, int offset) {
    Token<JavaTokenId>[] token = new Token[1];
    int[] span = getIdentifierSpan(doc, offset, token);

    if (span == null) {
      return null;
    }

    int exactOffset = controller.getSnapshot().getEmbeddedOffset(span[1] - 1);

    Element el = null;
    TypeMirror classType = null;
    TreePath path = controller.getTreeUtilities().pathFor(exactOffset);

    TreePath parent = path.getParentPath();

    if (parent != null) {
      Tree parentLeaf = parent.getLeaf();

      if (parentLeaf.getKind() == Kind.IMPORT && ((ImportTree) parentLeaf).isStatic()) {
        el = handleStaticImport(controller, (ImportTree) parentLeaf);
      }

      if (el == null) {
        el = controller.getTrees().getElement(path);

        if (parentLeaf.getKind() == Kind.METHOD_INVOCATION && isError(el)) {
          ExecutableElement ee = Utilities.fuzzyResolveMethodInvocation(controller, path.getParentPath(), new TypeMirror[1], new int[1]);

          if (ee != null) {
            el = ee;
          } else {
            ExpressionTree select = ((MethodInvocationTree) parentLeaf).getMethodSelect();
            Name methodName = null;
            switch (select.getKind()) {
              case IDENTIFIER:
                Scope s = controller.getTrees().getScope(path);
                el = s.getEnclosingClass();
                methodName = ((IdentifierTree) select).getName();
                break;
              case MEMBER_SELECT:
                el = controller.getTrees().getElement(new TreePath(path, ((MemberSelectTree) select).getExpression()));
                methodName = ((MemberSelectTree) select).getIdentifier();
                break;
            }
            if (el != null) {
              for (ExecutableElement m : ElementFilter.methodsIn(el.getEnclosedElements())) {
                if (m.getSimpleName() == methodName) {
                  el = m;
                  break;
                }
              }
            }
          }
        }
      }
    } else {
      return null;
    }

    if (isError(el)) {
      return null;
    }

    return new Context(classType, el);
  }

    private static String computeTooltip(CompilationInfo controller, Context resolved) {
        DisplayNameElementVisitor v = new DisplayNameElementVisitor(controller);

        if (resolved.resolved.getKind() == ElementKind.CONSTRUCTOR && resolved.classType != null && resolved.classType.getKind() == TypeKind.DECLARED) {
            v.printExecutable(((ExecutableElement) resolved.resolved), (DeclaredType) resolved.classType, true);
        } else  {
            v.visit(resolved.resolved, true);
        }

        String result = v.result.toString();
        result = "<html><body>" + result;

        return result;
    }
    
    private static final Set<JavaTokenId> USABLE_TOKEN_IDS = EnumSet.of(JavaTokenId.IDENTIFIER, JavaTokenId.DOT);
    // TODO dimen, attr, raw, anim, xml, style, animator, menu
    private static final Set<String> USABLE_RES_TYPES = Sets.newHashSet(
        "layout", "id", "string", "drawable", "styleable", "color");
    
    public static int[] getIdentifierSpan(final Document doc, final int offset, final Token<JavaTokenId>[] token) {
        if (EditorUtilities.getFileObject(doc) == null) {
            //do nothing if FO is not attached to the document - the goto would not work anyway:
            return null;
        }
        final int[][] ret = new int[][] {null}; 
        doc.render(new Runnable() {
            @Override
            public void run() {
                TokenHierarchy th = TokenHierarchy.get(doc);
                TokenSequence<JavaTokenId> ts = SourceUtils.getJavaTokenSequence(th, offset);

                if (ts == null)
                    return;

                ts.move(offset);
                if (!ts.moveNext())
                    return;

                Token<JavaTokenId> first = null;
                int firstIdx = -1;
                Token<JavaTokenId> t = ts.token();
                while (USABLE_TOKEN_IDS.contains(t.id())) {
                  if (!ts.movePrevious()) {
                    return;
                  }
                  first = t;
                  t = ts.token();
                  firstIdx = ts.offset() + t.length();
                }
                if (first == null) {
                  return;
                }

                // now look for IDENTIFIER("R"), DOT, IDENTIFIER(one of known values), DOT, IDENTIFIER(id)
                if (!"R".equals(first.text().toString())) {
                  return;
                }
                if (!ts.moveNext() || !JavaTokenId.IDENTIFIER.equals(ts.token().id()) ||
                    !ts.moveNext() || !JavaTokenId.DOT.equals(ts.token().id()) ||
                    !ts.moveNext() || !JavaTokenId.IDENTIFIER.equals(ts.token().id()) ||
                    !USABLE_RES_TYPES.contains(ts.token().text().toString()) ||
                    !ts.moveNext() || !JavaTokenId.DOT.equals(ts.token().id()) ||
                    !ts.moveNext() || !JavaTokenId.IDENTIFIER.equals(ts.token().id())) {
                  return;
                }
                Token<JavaTokenId> last = ts.token();
                if (token != null)
                    token[0] = last;

                ret[0] = new int [] {firstIdx, ts.offset() + last.length()};
            }
        });
        return ret[0];
    }
    
    /**
     * Tries to guess element referenced by static import. It may not be deterministic
     * as in <code>import static java.awt.Color.getColor</code>.
     */
    private static Element handleStaticImport(CompilationInfo javac, ImportTree impt) {
        Tree impIdent = impt.getQualifiedIdentifier();
        if (!impt.isStatic() || impIdent == null || impIdent.getKind() != Kind.MEMBER_SELECT) {
            return null;
        }
        
        // resolve type element containing imported element
        Trees trees = javac.getTrees();
        MemberSelectTree select = (MemberSelectTree) impIdent;
        Name mName = select.getIdentifier();
        TreePath cutPath = new TreePath(javac.getCompilationUnit());
        TreePath selectPath = new TreePath(new TreePath(cutPath, impt), select.getExpression());
        Element selectElm = trees.getElement(selectPath);
        if (isError(selectElm)) {
            return null;
        }
        
        // resolve class to determine scope
        TypeMirror clazzMir = null;
        TreePath clazzPath = null;
        List<? extends Tree> decls = javac.getCompilationUnit().getTypeDecls();
        if (!decls.isEmpty()) {
            Tree clazz = decls.get(0);
            if (TreeUtilities.CLASS_TREE_KINDS.contains(clazz.getKind())) {
                clazzPath = new TreePath(cutPath, clazz);
                Element clazzElm = trees.getElement(clazzPath);
                if (isError(clazzElm)) {
                    return null;
                }
                clazzMir = clazzElm.asType();
            }
        }
        if (clazzMir == null) {
            return null;
        }
        
        Scope clazzScope = trees.getScope(clazzPath);
        
        // choose the first acceptable member
        for (Element member : selectElm.getEnclosedElements()) {
            if (member.getModifiers().contains(Modifier.STATIC)
                    && mName.contentEquals(member.getSimpleName())
                    && javac.getTreeUtilities().isAccessible(clazzScope, member, clazzMir)) {
                return member;
            }
        }
        return null;
    }
    
    private static final class DisplayNameElementVisitor extends AbstractElementVisitor6<Void, Boolean> {

        private final CompilationInfo info;

        public DisplayNameElementVisitor(CompilationInfo info) {
            this.info = info;
        }
        
        private StringBuffer result        = new StringBuffer();
        
        private void boldStartCheck(boolean highlightName) {
            if (highlightName) {
                result.append("<b>");
            }
        }
        
        private void boldStopCheck(boolean highlightName) {
            if (highlightName) {
                result.append("</b>");
            }
        }
        
        @Override
        public Void visitPackage(PackageElement e, Boolean highlightName) {
            boldStartCheck(highlightName);
            
            result.append(e.getQualifiedName());
            
            boldStopCheck(highlightName);
            
            return null;
        }

        @Override
        public Void visitType(TypeElement e, Boolean highlightName) {
            return printType(e, null, highlightName);
        }
        
        Void printType(TypeElement e, DeclaredType dt, Boolean highlightName) {
            modifier(e.getModifiers());
            switch (e.getKind()) {
                case CLASS:
                    result.append("class ");
                    break;
                case INTERFACE:
                    result.append("interface ");
                    break;
                case ENUM:
                    result.append("enum ");
                    break;
                case ANNOTATION_TYPE:
                    result.append("@interface ");
                    break;
            }
            Element enclosing = e.getEnclosingElement();
            
            if (enclosing == SourceUtils.getEnclosingTypeElement(e)) {
                result.append(((TypeElement) enclosing).getQualifiedName());
                result.append('.');
                boldStartCheck(highlightName);
                result.append(e.getSimpleName());
                boldStopCheck(highlightName);
            } else {
                result.append(e.getQualifiedName());
            }
            
            if (dt != null)
                dumpRealTypeArguments(dt.getTypeArguments());

            return null;
        }

        @Override
        public Void visitVariable(VariableElement e, Boolean highlightName) {
            modifier(e.getModifiers());
            
            result.append(getTypeName(info, e.asType(), true));
            
            result.append(' ');
            
            boldStartCheck(highlightName);

            result.append(e.getSimpleName());
            
            boldStopCheck(highlightName);
            
            if (highlightName) {
                if (e.getConstantValue() != null) {
                    result.append(" = ");
                    result.append(e.getConstantValue().toString());
                }
                
                Element enclosing = e.getEnclosingElement();
                
                if (e.getKind() != ElementKind.PARAMETER && e.getKind() != ElementKind.LOCAL_VARIABLE
                        && e.getKind() != ElementKind.RESOURCE_VARIABLE && e.getKind() != ElementKind.EXCEPTION_PARAMETER) {
                    result.append(" in ");

                    //short typename:
                    result.append(getTypeName(info, enclosing.asType(), true));
                }
            }
            
            return null;
        }

        @Override
        public Void visitExecutable(ExecutableElement e, Boolean highlightName) {
            return printExecutable(e, null, highlightName);
        }

        Void printExecutable(ExecutableElement e, DeclaredType dt, Boolean highlightName) {
            switch (e.getKind()) {
                case CONSTRUCTOR:
                    modifier(e.getModifiers());
                    dumpTypeArguments(e.getTypeParameters());
                    result.append(' ');
                    boldStartCheck(highlightName);
                    result.append(e.getEnclosingElement().getSimpleName());
                    boldStopCheck(highlightName);
                    if (dt != null) {
                        dumpRealTypeArguments(dt.getTypeArguments());
                        dumpArguments(e.getParameters(), ((ExecutableType) info.getTypes().asMemberOf(dt, e)).getParameterTypes());
                    } else {
                        dumpArguments(e.getParameters(), null);
                    }
                    dumpThrows(e.getThrownTypes());
                    break;
                case METHOD:
                    modifier(e.getModifiers());
                    dumpTypeArguments(e.getTypeParameters());
                    result.append(getTypeName(info, e.getReturnType(), true));
                    result.append(' ');
                    boldStartCheck(highlightName);
                    result.append(e.getSimpleName());
                    boldStopCheck(highlightName);
                    dumpArguments(e.getParameters(), null);
                    dumpThrows(e.getThrownTypes());
                    break;
                case INSTANCE_INIT:
                case STATIC_INIT:
                    //these two cannot be referenced anyway...
            }
            return null;
        }

        @Override
        public Void visitTypeParameter(TypeParameterElement e, Boolean highlightName) {
            return null;
        }
        
        private void modifier(Set<Modifier> modifiers) {
            boolean addSpace = false;
            
            for (Modifier m : modifiers) {
                if (addSpace) {
                    result.append(' ');
                }
                addSpace = true;
                result.append(m.toString());
            }
            
            if (addSpace) {
                result.append(' ');
            }
        }
        
//        private void throwsDump()

        private void dumpTypeArguments(List<? extends TypeParameterElement> list) {
            if (list.isEmpty())
                return ;
            
            boolean addSpace = false;
            
            result.append("&lt;");
            
            for (TypeParameterElement e : list) {
                if (addSpace) {
                    result.append(", ");
                }
                
                result.append(getTypeName(info, e.asType(), true));
                
                addSpace = true;
            }
                
            result.append("&gt;");
        }

        private void dumpRealTypeArguments(List<? extends TypeMirror> list) {
            if (list.isEmpty())
                return ;

            boolean addSpace = false;

            result.append("&lt;");

            for (TypeMirror t : list) {
                if (addSpace) {
                    result.append(", ");
                }

                result.append(getTypeName(info, t, true));

                addSpace = true;
            }

            result.append("&gt;");
        }

        private void dumpArguments(List<? extends VariableElement> list, List<? extends TypeMirror> types) {
            boolean addSpace = false;
            
            result.append('(');

            Iterator<? extends VariableElement> listIt = list.iterator();
            Iterator<? extends TypeMirror> typesIt = types != null ? types.iterator() : null;

            while (listIt.hasNext()) {
                if (addSpace) {
                    result.append(", ");
                }
                
                VariableElement ve = listIt.next();
                TypeMirror      type = typesIt != null ? typesIt.next() : ve.asType();

                result.append(getTypeName(info, type, true));
                result.append(" ");
                result.append(ve.getSimpleName());

                addSpace = true;
            }
                
            result.append(')');
        }

        private void dumpThrows(List<? extends TypeMirror> list) {
            if (list.isEmpty())
                return ;
            
            boolean addSpace = false;
            
            result.append(" throws ");
            
            for (TypeMirror t : list) {
                if (addSpace) {
                    result.append(", ");
                }
                
                result.append(getTypeName(info, t, true));
                
                addSpace = true;
            }
        }
            
    }
    
    private static String getTypeName(CompilationInfo info, TypeMirror t, boolean fqn) {
        return translate(Utilities.getTypeName(info, t, fqn).toString());
    }
    
    private static String[] c = new String[] {"&", "<", ">", "\n", "\""}; // NOI18N
    private static String[] tags = new String[] {"&amp;", "&lt;", "&gt;", "<br>", "&quot;"}; // NOI18N
    
    private static String translate(String input) {
        for (int cntr = 0; cntr < c.length; cntr++) {
            input = input.replaceAll(c[cntr], tags[cntr]);
        }
        
        return input;
    }

    public static final class Context {
      public final TypeMirror classType;
      public final Element resolved;

      public Context(TypeMirror classType, Element resolved) {
        this.classType = classType;
        this.resolved = resolved;
      }

      @Override
      public String toString() {
        return "Context{" + "classType=" + classType + ", resolved=" + resolved + '}';
      }
    }
}
