package org.nbandroid.netbeans.ext.navigation;

import javax.lang.model.element.Element;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.modules.android.project.api.ResourceRef;
import org.openide.filesystems.FileObject;

/**
 *
 * @author radim
 */
public abstract class GoToContext {
  public final int offsetFrom;
  public final int offsetTo;
  
  public static GoToContext toResourceRef(int offsetFrom, int offsetTo, ResourceRef resolved) {
    return new ResourceContext(offsetFrom, offsetTo, resolved);
  }

  public static GoToContext toJavaElement(int offsetFrom, int offsetTo, 
      ClasspathInfo cpInfo, ElementHandle<? extends Element> hElem) {
    return new ElementContext(offsetFrom, offsetTo, cpInfo, hElem);
  }

  private static final class ElementContext extends GoToContext {
    
    public final ElementHandle<? extends Element> hElem;
    public final ClasspathInfo cpInfo;

    public ElementContext(int offsetFrom, int offsetTo, ClasspathInfo cpInfo, ElementHandle<? extends Element> hElem) {
      super(offsetFrom, offsetTo);
      this.hElem = hElem;
      this.cpInfo = cpInfo;
    }

    @Override
    public boolean open(UiUtilsCaller callback, FileObject fo) {
      return callback.open(cpInfo, hElem);
    }

    @Override
    public String getTooltipText() {
      return hElem.getQualifiedName();
    }

    @Override
    public String targetName() {
      return hElem.getQualifiedName();
    }

    @Override
    public String toString() {
      return "ElementContext{" + "offsetFrom=" + offsetFrom + ", offsetTo=" + offsetTo + "hElem=" + hElem + '}';
    }
  }

  private static final class ResourceContext extends GoToContext {

    public final ResourceRef resolved;

    public ResourceContext(int offsetFrom, int offsetTo, ResourceRef resolved) {
      super(offsetFrom, offsetTo);
      this.resolved = resolved;
    }

    @Override
    public boolean open(UiUtilsCaller callback, FileObject fo) {
      return callback.open(fo, resolved);
    }

    @Override
    public String getTooltipText() {
      return "";
    }

    @Override
    public String targetName() {
      return resolved.toRefString();
    }

    @Override
    public String toString() {
      return "ResourceContext{" + "offsetFrom=" + offsetFrom + ", offsetTo=" + offsetTo + ", resolved=" + resolved + '}';
    }
  }

  protected GoToContext(int offsetFrom, int offsetTo) {
    this.offsetFrom = offsetFrom;
    this.offsetTo = offsetTo;
  }

  public abstract boolean open(UiUtilsCaller callback, FileObject fo);
  public abstract String getTooltipText();
  public abstract String targetName();

  @Override
  public String toString() {
    return "GoToContext{" + "offsetFrom=" + offsetFrom + ", offsetTo=" + offsetTo + '}';
  }

}