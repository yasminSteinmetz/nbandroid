package org.nbandroid.netbeans.ext.navigation;

import com.google.common.base.Charsets;
import com.google.common.base.Predicate;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.io.Resources;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.text.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.openide.filesystems.FileObject;

import org.netbeans.api.java.source.SourceUtilsTestUtil;
import org.netbeans.api.java.source.TestUtilities;
import org.netbeans.api.lexer.Language;
import org.netbeans.junit.MockServices;
import static org.netbeans.modules.android.project.AndroidTestFixture.SDK_DIR;
import org.netbeans.modules.android.project.FileUtilities;
import org.netbeans.modules.java.source.TreeLoader;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.netbeans.modules.android.project.api.ResourceRef;

public class AndroidHyperlinkProviderTest {
  
  private File tempFolder;
  private FileObject scratch;
  private FileObject sdkDirFo;
  private FileObject source;

  @Before
  public void setUp() throws Exception {
    SourceUtilsTestUtil.prepareTest(new String[]{"org/netbeans/modules/java/editor/resources/layer.xml"}, new Object[0]);
    org.netbeans.api.project.ui.OpenProjects.getDefault().getOpenProjects();
    TreeLoader.DISABLE_ARTIFICAL_PARAMETER_NAMES = true;
    
    MockServices.setServices();
    tempFolder = File.createTempFile("junit", "");
    tempFolder.delete();
    tempFolder.mkdir();

    scratch = FileUtil.toFileObject(tempFolder);
    sdkDirFo = FileUtil.toFileObject(new File(SDK_DIR));
  }

  @After
  public void tearDown() {
    FileUtilities.recursiveDelete(tempFolder);
  }
  
  @Test
  public void testSkeletonActivity() throws Exception {
    final String sourceTxt = 
        Resources.toString(AndroidHyperlinkProviderTest.class.getResource("SkeletonActivity.txt"), Charsets.UTF_8);
    Document doc = prepareTest(
        sourceTxt,
        false);
    List<String> links = Lists.newArrayList(
      "R.layout.skeleton_activity",
      "R.id.editor",
      "R.id.back",
      "R.id.clear",
      "R.string.main_label",
      "R.string.back",
      "R.string.clear"
    );
    for (int i = 0; i < doc.getLength(); i++) {
      final int current = i;
      String id = Iterables.find(links, new Predicate<String>() {
            @Override
            public boolean apply(String t) {
              int idx = sourceTxt.indexOf(t);
              return current >= idx && current < idx + t.length();
            }
          },
          null);
      boolean expectHyperlink = id != null;
      UiUtilsCaller callback = mock(UiUtilsCaller.class);
      ArrayList<String> refParts = null;
      if (expectHyperlink) {
        refParts = Lists.newArrayList(Splitter.on('.').split(id));
        when(callback.open(eq(source), eq(new ResourceRef(true, "com.example.android.skeletonapp", refParts.get(1), refParts.get(2), null)))).thenReturn(true);
      }
      performTest(doc, i, callback, expectHyperlink);
      if (expectHyperlink) {
        verify(callback).open(eq(source), eq(new ResourceRef(true, "com.example.android.skeletonapp", refParts.get(1), refParts.get(2), null)));
      } else {
        verifyZeroInteractions(callback);
      }
    }
  }
  
  @Test
  public void testSnakeResources() throws Exception {
    final String sourceTxt = 
        Resources.toString(AndroidHyperlinkProviderTest.class.getResource("Snake.txt"), Charsets.UTF_8);
    Document doc = prepareTest(
        sourceTxt,
        false);
    List<String> links = Lists.newArrayList(
        "R.layout.snake_layout",
        "R.id.snake",
        "R.id.text",
        "R.drawable.redstar",
        "R.drawable.yellowstar",
        "R.drawable.greenstar",
        "R.string.mode_pause",
        "R.string.mode_ready",
        "R.string.mode_lose_prefix",
        "R.string.mode_lose_suffix",
        "R.styleable.TileView_tileSize",
        "R.styleable.TileView");
    for (int i = 0; i < doc.getLength(); i++) {
      final int current = i;
      String id = Iterables.find(links, new Predicate<String>() {
            @Override
            public boolean apply(String t) {
              int idx = sourceTxt.indexOf(t, Math.max(0, current - t.length()));
              return current >= idx && current < idx + t.length();
            }
          },
          null);
      boolean expectHyperlink = id != null;
      UiUtilsCaller callback = mock(UiUtilsCaller.class);
      ArrayList<String> refParts = null;
      if (expectHyperlink) {
        refParts = Lists.newArrayList(Splitter.on('.').split(id));
        when(callback.open(eq(source), eq(new ResourceRef(true, "com.example.android.snake", refParts.get(1), refParts.get(2), null)))).thenReturn(true);
      }
      performTest(doc, i, callback, expectHyperlink);
      if (expectHyperlink) {
        verify(callback).open(eq(source), eq(new ResourceRef(true, "com.example.android.snake", refParts.get(1), refParts.get(2), null)));
      } else {
        verifyZeroInteractions(callback);
      }
    }
  }

  private Document prepareTest(
      String sourceCode, boolean doCompileRecursively) throws Exception {
    FileUtil.refreshFor(tempFolder);

    FileObject sourceDir = FileUtil.createFolder(scratch, "src");
    FileObject buildDir = FileUtil.createFolder(scratch, "build");
    FileObject cacheDir = FileUtil.createFolder(scratch, "cache");

    source = FileUtil.createData(sourceDir, "test/Test.java");

    TestUtilities.copyStringToFile(source, sourceCode);

    SourceUtilsTestUtil.prepareTest(sourceDir, buildDir, cacheDir, new FileObject[0]);

    if (doCompileRecursively) {
      SourceUtilsTestUtil.compileRecursively(sourceDir);
    }

    DataObject od = DataObject.find(source);
    EditorCookie ec = od.getCookie(EditorCookie.class);
    Document doc = ec.openDocument();

    doc.putProperty(Language.class, JavaTokenId.language());
    doc.putProperty("mimeType", "text/x-java");
    return doc;
  }

  private void performTest(
      Document doc, final int offset, final UiUtilsCaller validator, boolean isHyperlink) throws Exception {
    String sample = doc.getText(offset, Math.min(25, doc.getLength() - offset));
    if (isHyperlink) {
      assertNotNull("hyperlink at offset " + offset + ": " + sample, GoToSupport.getIdentifierSpan(doc, offset, null));
      new GoToSupport(validator).goTo(doc, offset);
    } else {
      assertNull("no hyperlink at offset " + offset + ": " + sample, GoToSupport.getIdentifierSpan(doc, offset, null));
    }
  }
}
