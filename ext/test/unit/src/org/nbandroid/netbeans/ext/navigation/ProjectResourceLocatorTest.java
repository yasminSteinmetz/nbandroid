package org.nbandroid.netbeans.ext.navigation;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.core.sdk.DalvikPlatformManager;
import org.netbeans.modules.android.project.AndroidProject;
import org.netbeans.modules.android.project.AndroidTestFixture;
import org.openide.filesystems.FileObject;

import static org.junit.Assert.*;
import org.netbeans.modules.android.project.api.ResourceRef;

/**
 *
 * @author radim
 */
public class ProjectResourceLocatorTest {
  
  private static AndroidTestFixture fixture;
  private static FileObject skeletonPrjDir;
  private static AndroidProject skeletonPrj;
  private static FileObject snakePrjDir;
  private static AndroidProject snakePrj;
  private static FileObject someSource1;
  private static FileObject skeletonLayout;
  private static FileObject someLayout1;
  private static DalvikPlatform testedPlatform;

  @BeforeClass
  public static void setUpClass() throws Exception {
    fixture = AndroidTestFixture.create().withProject("Snake", "samples/android-8/Snake")
        .withProject("SkeletonApp", "samples/android-8/SkeletonApp");

    snakePrjDir = fixture.getProjectFolder("Snake");
    snakePrj = (AndroidProject) fixture.getProject("Snake");
    someSource1 = snakePrjDir.getFileObject("src/com/example/android/snake/Snake.java");
    someLayout1 = snakePrjDir.getFileObject("res/layout/snake_layout.xml");

    skeletonPrjDir = fixture.getProjectFolder("SkeletonApp");
    skeletonPrj = (AndroidProject) fixture.getProject("SkeletonApp");
    skeletonLayout = skeletonPrjDir.getFileObject("res/layout/skeleton_activity.xml");
    testedPlatform = DalvikPlatformManager.getDefault().findPlatformForTarget("android-8");
  }

  @AfterClass
  public static void delete() {
    fixture.tearDown();
  }
  
  @Test
  public void drawableName() {
    ResourceLocator resLocator = snakePrj.getLookup().lookup(ResourceLocator.class);
    FileObject redstar = snakePrjDir.getFileObject("res/drawable/redstar.png");
    ResourceLocation resourceLoc = resLocator.findResourceLocation(
        new ResourceRef(true, "com.example.android.snake", "drawable", "redstar", null));
    assertEquals(redstar, resourceLoc.resource);
  }
  
  @Test
  public void drawableJpg() {
    ResourceLocator resLocator = skeletonPrj.getLookup().lookup(ResourceLocator.class);
    FileObject violet = skeletonPrjDir.getFileObject("res/drawable/violet.jpg");
    ResourceLocation resourceLoc = resLocator.findResourceLocation(
        new ResourceRef(true, "com.example.android.skeletonapp", "drawable", "violet", null));
    assertEquals(violet, resourceLoc.resource);
  }
  
  @Test
  public void drawableInColorsXml() {
    ResourceLocator resLocator = skeletonPrj.getLookup().lookup(ResourceLocator.class);
    FileObject colors = skeletonPrjDir.getFileObject("res/values/colors.xml");
    ResourceLocation resourceLoc = resLocator.findResourceLocation(
        new ResourceRef(true, "com.example.android.skeletonapp", "drawable", "semi_black", null));
    assertEquals(colors, resourceLoc.resource);
    assertEquals(26, resourceLoc.line);
  }
  
  @Test
  public void color() {
    ResourceLocator resLocator = skeletonPrj.getLookup().lookup(ResourceLocator.class);
    FileObject colors = skeletonPrjDir.getFileObject("res/values/colors.xml");
    ResourceLocation resourceLoc = resLocator.findResourceLocation(
        new ResourceRef(true, "com.example.android.skeletonapp", "color", "red", null));
    assertEquals(colors, resourceLoc.resource);
    assertEquals(23, resourceLoc.line);
  }
  
  @Test
  public void layoutName() {
    ResourceLocator resLocator = snakePrj.getLookup().lookup(ResourceLocator.class);
    ResourceLocation resourceLoc = resLocator.findResourceLocation(
        new ResourceRef(true, "com.example.android.skeletonapp", "layout", "snake_layout", null));
    assertEquals(someLayout1, resourceLoc.resource);
  }
  
  @Test
  public void idInLayout() {
    ResourceLocator resLocator = skeletonPrj.getLookup().lookup(ResourceLocator.class);
    // TODO 32 is better (tag spans 32-37 and id is on 32)
    ResourceLocation resourceLoc = resLocator.findResourceLocation(
        new ResourceRef(true, "com.example.android.skeletonapp", "id", "editor", null));
    assertEquals(skeletonLayout, resourceLoc.resource);
    assertEquals(36, resourceLoc.line);
    
    resourceLoc = resLocator.findResourceLocation(
        new ResourceRef(true, "com.example.android.skeletonapp", "id", "back", null));
    assertEquals(skeletonLayout, resourceLoc.resource);
    assertEquals(55, resourceLoc.line);
    
    resourceLoc = resLocator.findResourceLocation(
        new ResourceRef(true, "com.example.android.skeletonapp", "id", "clear", null));
    assertEquals(skeletonLayout, resourceLoc.resource);
    assertEquals(66, resourceLoc.line);
  }
  
  @Test
  public void strings() {
    ResourceLocator resLocator = skeletonPrj.getLookup().lookup(ResourceLocator.class);
    FileObject strings = skeletonPrjDir.getFileObject("res/values/strings.xml");
    // TODO 32 is better (tag spans 32-37 and id is on 32)
    ResourceLocation resourceLoc = resLocator.findResourceLocation(
        new ResourceRef(true, "com.example.android.skeletonapp", "string", "main_label", null));
    assertEquals(strings, resourceLoc.resource);
    assertEquals(26, resourceLoc.line);

    resourceLoc = resLocator.findResourceLocation(
        new ResourceRef(true, "com.example.android.skeletonapp", "string", "back", null));
    assertEquals(strings, resourceLoc.resource);
    assertEquals(22, resourceLoc.line);

    resourceLoc = resLocator.findResourceLocation(
        new ResourceRef(true, "com.example.android.skeletonapp", "string", "clear", null));
    assertEquals(strings, resourceLoc.resource);
    assertEquals(23, resourceLoc.line);
  }
  
  @Test
  public void styleables() {
    ResourceLocator resLocator = snakePrj.getLookup().lookup(ResourceLocator.class);
    FileObject attrs = snakePrjDir.getFileObject("res/values/attrs.xml");
    ResourceLocation resourceLoc = resLocator.findResourceLocation(
        new ResourceRef(true, "com.example.android.skeletonapp", "styleable", "TileView", null));
    assertEquals(attrs, resourceLoc.resource);
    assertEquals(17, resourceLoc.line);

    resourceLoc = resLocator.findResourceLocation(
        new ResourceRef(true, "com.example.android.skeletonapp", "styleable", "TileView_tileSize", null));
    assertEquals(attrs, resourceLoc.resource);
    assertEquals(18, resourceLoc.line);
  }
}