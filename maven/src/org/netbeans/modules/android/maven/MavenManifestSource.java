/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.netbeans.modules.android.maven;

import com.google.common.base.Preconditions;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.project.Project;
import org.netbeans.modules.android.project.api.AndroidConstants;
import org.netbeans.modules.android.project.api.AndroidManifestSource;
import org.netbeans.modules.maven.api.PluginPropertyUtils;
import org.netbeans.spi.project.ProjectServiceProvider;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

/**
 *
 * @author radim
 */
@ProjectServiceProvider(service = AndroidManifestSource.class,
    projectType = "org-netbeans-modules-maven")
public class MavenManifestSource implements AndroidManifestSource {
  private static final Logger LOG = Logger.getLogger(MavenManifestSource.class.getName());

  private final Project project;

  public MavenManifestSource(Project project) {
    this.project = Preconditions.checkNotNull(project);
  }

  @Override
  public FileObject get() {
    String pluginProperty = PluginPropertyUtils.getPluginProperty(
        project, 
        "com.jayway.maven.plugins.android.generation2", 
        "android-maven-plugin", 
        "androidManifestFile", 
        null);
    LOG.log(Level.FINE, "androidManifestFile property: {0}", pluginProperty);
    if (pluginProperty == null) {
      // try fallback to AndroidManifest.xml in project root directory
      return project.getProjectDirectory().getFileObject(AndroidConstants.ANDROID_MANIFEST_XML);
    }
    File manifestFile = new File(pluginProperty);
    return FileUtil.toFileObject(manifestFile);
  }
  
}
