/*
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package org.netbeans.modules.android.project.queries;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nbandroid.netbeans.test.TestPlatform;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.modules.android.core.sdk.DalvikPlatformManager;
import org.netbeans.modules.android.project.AndroidProject;
import org.netbeans.modules.android.project.FileUtilities;
import org.netbeans.modules.android.project.TestUtils;
import org.netbeans.spi.java.classpath.ClassPathProvider;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author radim, Baldur
 */
public class ClassPathProviderImplTest {

  private static final String SDK_DIR = System.getProperty("test.all.android.sdks.home");

  private static DalvikPlatformManager platformManager;

  private static File tempFolder;

  @BeforeClass
  public static void setUpClass() throws Exception {
    platformManager = DalvikPlatformManager.getDefault();
    platformManager.setSdkLocation(SDK_DIR);
  }

  @After
  public void clear() {
    FileUtilities.recursiveDelete(tempFolder);
  }

  @Test
  public void sourceDir() throws Exception {
    tempFolder = File.createTempFile("junit", "");
    tempFolder.delete();
    tempFolder.mkdir();
    TestUtils.unzip(ClassPathProviderImplTest.class.getResourceAsStream("alternativeSourceDir.zip"), tempFolder);
    File prjDir = new File(tempFolder, "AndroidApplication2");
    Project prj = ProjectManager.getDefault().findProject(FileUtil.toFileObject(prjDir));
    assertNotNull(prj);
    FileObject foProjectSrc = prj.getProjectDirectory().getFileObject("sources");

    // get the classpath
    ClassPathProvider cpp = prj.getLookup().lookup(ClassPathProvider.class);
    ClassPath classpath = cpp.findClassPath(foProjectSrc, ClassPath.SOURCE);
    assertNotNull("source.dir property from ant.properties file is honored", classpath);
    // verify it
    assertNotNull("source.dir property from ant.properties file is honored", classpath.toString());
  }

  @Test
  public void extSourceDir() throws Exception {
    tempFolder = File.createTempFile("junit", "");
    tempFolder.delete();
    tempFolder.mkdir();
    TestUtils.unzip(ClassPathProviderImplTest.class.getResourceAsStream("extSourceDir.zip"), tempFolder);
    File prjDir = new File(tempFolder, "AndroidApplication2");
    Project prj = ProjectManager.getDefault().findProject(FileUtil.toFileObject(prjDir));
    assertNotNull(prj);
    FileObject foProjectSrc = prj.getProjectDirectory().getFileObject("../sources");

    // get the classpath
    ClassPathProvider cpp = prj.getLookup().lookup(ClassPathProvider.class);
    ClassPath classpath = cpp.findClassPath(foProjectSrc, ClassPath.SOURCE);
    assertNotNull("source.dir property from ant.properties file is honored", classpath);
    // verify it
    assertNotNull("source.dir property from ant.properties file is honored", classpath.toString());
    Project ownerPrj = FileOwnerQuery.getOwner(foProjectSrc);
    assertEquals(prj, ownerPrj);
  }

  @Test
  public void moreExtSourceDirs() throws Exception {
    tempFolder = File.createTempFile("junit", "");
    tempFolder.delete();
    tempFolder.mkdir();
    TestUtils.unzip(ClassPathProviderImplTest.class.getResourceAsStream("extSources2Dir.zip"), tempFolder);
    File prjDir = new File(tempFolder, "AndroidApplication2");
    Project prj = ProjectManager.getDefault().findProject(FileUtil.toFileObject(prjDir));
    assertNotNull(prj);
    FileObject foProjectSrc = prj.getProjectDirectory().getFileObject("../sources");

    // get the classpath
    ClassPathProvider cpp = prj.getLookup().lookup(ClassPathProvider.class);
    ClassPath classpath = cpp.findClassPath(foProjectSrc, ClassPath.SOURCE);
    assertNotNull("source.dir property from ant.properties file is honored", classpath);
    // verify it
    assertNotNull("source.dir property from ant.properties file is honored", classpath.toString());
    Project ownerPrj = FileOwnerQuery.getOwner(foProjectSrc);
    assertEquals(prj, ownerPrj);
    
    foProjectSrc = prj.getProjectDirectory().getFileObject("../sources2");

    // get the classpath
    classpath = cpp.findClassPath(foProjectSrc, ClassPath.SOURCE);
    assertNotNull("source.dir property from ant.properties file is honored", classpath);
    // verify it
    assertNotNull("source.dir property from ant.properties file is honored", classpath.toString());
    ownerPrj = FileOwnerQuery.getOwner(foProjectSrc);
    assertEquals(prj, ownerPrj);
  }

  @Test
  public void classPathWithLibraryProjectRecursion() throws IOException {
    tempFolder = File.createTempFile("junit", "");
    tempFolder.delete();
    tempFolder.mkdirs();

    // file object for the temp dir
    FileObject root = FileUtil.toFileObject(tempFolder);
    FileObject foProject = TestUtils.createProject(TestPlatform.ANDROID_4_0_3, root, "TestProject", false, "TestLibrary");
    // beware: there is circular dependency on the library project!!!
    FileObject foFirstLib = TestUtils.createProject(TestPlatform.ANDROID_4_0_3, root, "TestLibrary", true, "TestLibrary");

    // create the project
    AndroidProject project = (AndroidProject)ProjectManager.getDefault().findProject(foProject);
    ClassPathProvider cpp = project.getLookup().lookup(ClassPathProvider.class);
    assertNotNull(cpp);

    // expected paths
    FileObject foProjectSrc = foProject.getFileObject("src");
    FileObject foFirstLibSrc = foFirstLib.getFileObject("src"); // no longer used
    FileObject foFirstLibJar = foFirstLib.getFileObject("bin/classes.jar"); // no longer used

    // get the classpath
    ClassPath classpath = cpp.findClassPath(foProjectSrc, ClassPath.SOURCE);
    assertNotNull("classpath for path " + foProjectSrc.getPath() + " not found.", classpath);

    List<String> cpEntries = Lists.newArrayList(
        Splitter.on(':').split(classpath.toString(ClassPath.PathConversionMode.PRINT)));
    assertTrue("Classpath " + classpath + " has sources", cpEntries.contains(foProjectSrc.getPath()));
    assertFalse("Classpath " + classpath + " has not lib sources", cpEntries.contains(foFirstLibSrc.getPath()));

    ClassPath compileClasspath = cpp.findClassPath(foProjectSrc, ClassPath.COMPILE);
    assertNotNull("classpath for path " + foProjectSrc.getPath() + " not found.", compileClasspath);
    List<String> compileCpEntries = Lists.newArrayList(
        Splitter.on(':').split(compileClasspath.toString(ClassPath.PathConversionMode.PRINT)));
    assertFalse("Classpath " + classpath + " has sources", compileCpEntries.contains(foProjectSrc.getPath()));
    assertTrue("Classpath " + classpath + " has lib sources", compileCpEntries.contains(foFirstLibJar.getPath()));
  }
}
