/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.netbeans.modules.android.grammars;

import java.util.Set;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.core.sdk.DalvikPlatformManager;
import org.netbeans.modules.android.core.sdk.StyleableModel;
import org.netbeans.modules.android.project.api.AndroidProjects;
import static org.junit.Assert.*;
import org.nbandroid.netbeans.test.AbstractAndroidPlatformTest;
import org.nbandroid.netbeans.test.PlatformVersions;

@PlatformVersions
public class StyleableModelsTest extends AbstractAndroidPlatformTest {

  private static final String SDK_DIR = System.getProperty("test.all.android.sdks.home");

  @BeforeClass
  public static void classSetup() {
    DalvikPlatformManager.getDefault().setSdkLocation(SDK_DIR);
  }

  @Test
  public void testAndroidLayoutsModel() throws Exception {
    DalvikPlatform platform = DalvikPlatformManager.getDefault().findPlatformForTarget(getPlatform().target);
    assertNotNull(platform);

    StyleableModel manifestModel = 
            AndroidLayoutGrammar.create(platform, AndroidProjects.noReferenceResolver()).getStyleableModel();
    assertNotNull(manifestModel);
    Set<String> names = manifestModel.getStyleables().keySet();
    assertTrue(!names.isEmpty());
    assertNotNull("There are styleables in " + platform, 
        manifestModel.getStyleables());
    assertNotNull("Theme is regonized " + platform, 
        manifestModel.getStyleables().get("Theme"));
    assertNotNull("Theme has description in " + platform, 
        manifestModel.getStyleables().get("Theme").getDescription());
    assertNotSame("Descriptions in " + platform + " are not the same", 
        manifestModel.getStyleables().get("Theme").getDescription(),
        manifestModel.getStyleables().get("Window").getDescription());
  }
}
