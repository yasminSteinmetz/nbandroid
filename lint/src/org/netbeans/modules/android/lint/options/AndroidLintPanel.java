/*
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package org.netbeans.modules.android.lint.options;

import com.android.tools.lint.checks.BuiltinIssueRegistry;
import com.android.tools.lint.detector.api.Category;
import com.android.tools.lint.detector.api.Issue;
import java.awt.Component;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.swing.JCheckBox;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.netbeans.modules.android.lint.Registry;
import org.netbeans.modules.android.lint.Utilities;
import org.netbeans.modules.android.lint.options.OptionsFilterHack.Acceptor;
import org.openide.util.Exceptions;

/**
 * Parts based on HintsPanel and HintsPanelLogic from java.hints:
 * 
 * @author lahoda@gmail.com
 */
final class AndroidLintPanel extends javax.swing.JPanel {

    private final AndroidLintOptionsPanelController controller;
    private ModifiedPreferences prefs;

    AndroidLintPanel(AndroidLintOptionsPanelController controller, OptionsFilterHack filter) {
        this.controller = controller;
        initComponents();
        prefs = new ModifiedPreferences(Registry.getPreferences());
        final Map<Category, Collection<Issue>> categorized = categorize();
        TreeModel baseModel = new DefaultTreeModel(create(categorized));
        if (filter == null /*filter != null*/|| !filter.installFilteringModel(issues, baseModel, new AcceptorImpl())) {
//            filter.installFilteringModel(issues, baseModel, new AcceptorImpl());
//        } else {
            issues.setModel(baseModel);
        }
        issues.setCellRenderer(new CheckBoxRenderer(categorized));
        issues.setRootVisible(false);
        issues.setShowsRootHandles(true);
        issues.addTreeSelectionListener(new TreeSelectionListener() {
            @Override public void valueChanged(TreeSelectionEvent e) {
                DefaultMutableTreeNode leaf = (DefaultMutableTreeNode) e.getPath().getLastPathComponent();
                Object data = leaf.getUserObject();
                updateDescription(data);
            }
        });
        issues.addMouseListener(new MouseAdapter() {
            @Override public void mouseClicked(MouseEvent e) {
                TreePath path = issues.getSelectionPath();
                
                if (path == null) return;
                
                DefaultMutableTreeNode leaf = (DefaultMutableTreeNode) path.getLastPathComponent();
                Object data = leaf.getUserObject();
                
                Rectangle rowBounds = issues.getRowBounds(issues.getRowForPath(path));
                
                rowBounds.width = rowBounds.height;
                
                if (rowBounds.contains(issues.getMousePosition())) {
                    if (data instanceof Issue) {
                        Issue issue = (Issue) data;
                        
                        Registry.setEnabled(prefs, issue.getId(), !Registry.isEnabled(prefs, issue.getId()));
                    } else if (data instanceof Category) {
                        Category cat = (Category) data;
                        boolean enabledTarget = !categoryEnabled(categorized, cat);
                        
                        for (Issue issue : categorized.get(cat)) {
                            Registry.setEnabled(prefs, issue.getId(), enabledTarget);
                        }
                        
                    }
                    
                    //TODO: specify a smaller repaint rectangle?
                    issues.repaint();
                }
            }
        });
        
        split.setDividerLocation(0.5);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        split = new javax.swing.JSplitPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        issues = new javax.swing.JTree();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        description = new javax.swing.JTextPane();
        runInEditor = new javax.swing.JCheckBox();

        jScrollPane1.setViewportView(issues);

        split.setLeftComponent(jScrollPane1);

        org.openide.awt.Mnemonics.setLocalizedText(jLabel1, org.openide.util.NbBundle.getMessage(AndroidLintPanel.class, "AndroidLintPanel.jLabel1.text")); // NOI18N

        description.setEditable(false);
        jScrollPane2.setViewportView(description);

        org.openide.awt.Mnemonics.setLocalizedText(runInEditor, org.openide.util.NbBundle.getMessage(AndroidLintPanel.class, "AndroidLintPanel.runInEditor.text")); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(runInEditor)
                            .addComponent(jLabel1))
                        .addGap(0, 83, Short.MAX_VALUE))
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(runInEditor)
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)
                .addContainerGap())
        );

        split.setRightComponent(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(split, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(split, javax.swing.GroupLayout.DEFAULT_SIZE, 68, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    void load() {
        prefs = new ModifiedPreferences(Registry.getPreferences());
    }

    void store() {
        prefs.store(Registry.getPreferences());
    }

    boolean valid() {
        // TODO check whether form is consistent and complete
        return true;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextPane description;
    private javax.swing.JTree issues;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JCheckBox runInEditor;
    private javax.swing.JSplitPane split;
    // End of variables declaration//GEN-END:variables

    private static Map<Category, Collection<Issue>> categorize() {
        Map<Category, Collection<Issue>> categorized = new TreeMap<Category, Collection<Issue>>(new Comparator<Category>() {
            @Override public int compare(Category o1, Category o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        
        for (Issue i : new BuiltinIssueRegistry().getIssues()) {
            Collection<Issue> inside = categorized.get(i.getCategory());
            
            if (inside == null) {
                categorized.put(i.getCategory(), inside = new TreeSet<Issue>(new Comparator<Issue>() {
                    @Override public int compare(Issue o1, Issue o2) {
                        return Utilities.displayName(o1).compareTo(Utilities.displayName(o2));
                    }
                }));
            }
            
            inside.add(i);
        }
        
        return categorized;
    }
    
    private static DefaultMutableTreeNode create(Map<Category, Collection<Issue>> categorized) {
        
        DefaultMutableTreeNode root = new DefaultMutableTreeNode();
        
        for (Entry<Category, Collection<Issue>> e : categorized.entrySet()) {
            DefaultMutableTreeNode cat = new DefaultMutableTreeNode(e.getKey());
            
            for (Issue i : e.getValue()) {
                DefaultMutableTreeNode issueNode = new DefaultMutableTreeNode(i);
                
                cat.add(issueNode);
            }
            
            root.add(cat);
        }
        
        return root;
    }
    

    private void updateDescription(Object data) {
        if (data instanceof Category) {
            description.setText(((Category) data).getExplanation());
            runInEditor.setSelected(false);
            runInEditor.setEnabled(false);
        } else if (data instanceof Issue) {
            Issue issue = (Issue) data;
            StringBuilder descriptionBuilder = new StringBuilder();
            
            descriptionBuilder.append(issue.getDescription());
            descriptionBuilder.append("\n");
            descriptionBuilder.append(issue.getExplanation());
            
            if (Registry.isSupportedInEditor(issue.getId())) {
                runInEditor.setEnabled(true);
                runInEditor.setSelected(Registry.isEnabledInEditor(prefs, issue.getId()));
            } else {
                runInEditor.setEnabled(false);
                runInEditor.setSelected(false);
                descriptionBuilder.append("\n\n");
                descriptionBuilder.append("This lint cannot be run in editor.");
            }
            
            description.setText(descriptionBuilder.toString());
        }
    }
            
    private boolean categoryEnabled(Map<Category, Collection<Issue>> categorized, Category category) {
        for (Issue issue : categorized.get(category)) {
            if (Registry.isEnabled(prefs, issue.getId())) {
                return true;
            }
        }

        return false;
    }
        
    class CheckBoxRenderer implements TreeCellRenderer {
    
        private final JCheckBox renderer = new JCheckBox();
        private final DefaultTreeCellRenderer dr = new DefaultTreeCellRenderer();
        private final Map<Category, Collection<Issue>> categorized;

        public CheckBoxRenderer(Map<Category, Collection<Issue>> categorized) {
            this.categorized = categorized;
        }

        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {

            renderer.setBackground(selected ? dr.getBackgroundSelectionColor() : dr.getBackgroundNonSelectionColor());
            renderer.setForeground(selected ? dr.getTextSelectionColor() : dr.getTextNonSelectionColor());
            renderer.setFont(renderer.getFont().deriveFont(Font.PLAIN));

            Object data = ((DefaultMutableTreeNode) value).getUserObject();
            if (data instanceof Category) {
                Category cat = ((Category) data);
                boolean enabled = categoryEnabled(categorized, cat);
                
                renderer.setText(cat.getName());
                renderer.setSelected(enabled);
            } else if (data instanceof Issue) {
                Issue issue = (Issue) data;
                
                renderer.setText(Utilities.displayName(issue));
                renderer.setSelected(Registry.isEnabled(prefs, issue.getId()));
            }

            return renderer;
        }
    }
    
    private static class ModifiedPreferences extends AbstractPreferences {
        
        private Map<String,Object> map = new HashMap<String, Object>();

        public ModifiedPreferences( Preferences node ) {
            super(null, ""); // NOI18N
            try {                
                for (java.lang.String key : node.keys()) {
                    put(key, node.get(key, null));
                }
            }
            catch (BackingStoreException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
             
        
        public void store( Preferences target ) {
            
            try {
                for (String key : keys()) {
                    target.put(key, get(key, null));
                }
            }
            catch (BackingStoreException ex) {
                Exceptions.printStackTrace(ex);
            }

        }
        
        protected void putSpi(String key, String value) {
            map.put(key, value);            
        }

        protected String getSpi(String key) {
            return (String)map.get(key);                    
        }

        protected void removeSpi(String key) {
            map.remove(key);
        }

        protected void removeNodeSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        protected String[] keysSpi() throws BackingStoreException {
            String array[] = new String[map.keySet().size()];
            return map.keySet().toArray( array );
        }

        protected String[] childrenNamesSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        protected AbstractPreferences childSpi(String name) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        protected void syncSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        protected void flushSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

	boolean isEmpty() {
	    return map.isEmpty();
	}
    }

    private static class AcceptorImpl implements Acceptor {

        @Override
        public boolean accept(Object originalTreeNode, String filterText) {
            filterText = filterText.toLowerCase().trim();
                
            if (filterText.isEmpty()) return true;
            
            Object data = ((DefaultMutableTreeNode) originalTreeNode).getUserObject();
            
            if (data instanceof Issue) {
                Issue issue = (Issue) data;
                
                if (check(issue.getDescription(), filterText)) return true;
                if (check(issue.getExplanation(), filterText)) return true;
                if (check(issue.getMoreInfo(), filterText)) return true;
                if (check(issue.getId(), filterText)) return true;
                
                System.err.println("id=" + issue.getId() + ":" + issue.getScope());
                
                return false;
            }
            
            return false;
        }
        
        private boolean check(String text, String filterText) {
            return text != null && text.toLowerCase().contains(filterText);
        }
    }
}
